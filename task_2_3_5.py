def is_prime(x):
    if x % 2 and x > 2:
        for i in range(3, int(x**0.5) + 1, 2):
            if x % i == 0:
                return False
    elif x != 2:
        return False
    return True

def primes():
    for i in range(999999):
        if is_prime(i):
            yield i