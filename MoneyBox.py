class MoneyBox:
    def __init__(self, capacity):
        self.capacity = capacity
        self.value = 0
        #конструктор с аргументом – вместимость копилки

    def can_add(self, v):
        #True, если можно добавить v монет, False иначе
        if v <= self.capacity-self.value:
            return True
        else:
            return False

    def add(self, v):
        if self.can_add(v):
            self.value += v
        else:
            pass
        #положить v монет в копилку