# ВЫПОЛНЕНО
import requests
import json
import operator

art_list = list()
with open("art.txt") as f:
    for line in f:
        art_list.append(line.rstrip())

client_id = '4cdd5498e0f32a328388'
client_secret = '026ad7598feafb82695ab4070d58331b'

# инициируем запрос на получение токена
r = requests.post("https://api.artsy.net/api/tokens/xapp_token",
                  data={
                      "client_id": client_id,
                      "client_secret": client_secret
                  })

# разбираем ответ сервера
j = json.loads(r.text)
token = j["token"]

#print(token)

# создаем заголовок, содержащий наш токен
headers = {"X-Xapp-Token" : token}

art_list_data = list()
# инициируем запрос с заголовком
for x in art_list:
    r = requests.get("https://api.artsy.net/api/artists/"+x, headers=headers)
    i = json.loads(r.text)
    td = (i['sortable_name'], int(i['birthday']))
    art_list_data.append(td)

for key in art_list_data:
    print(key[1], key[0])

art_list_data.sort(key = operator.itemgetter(1,0))
print("============")
for key in art_list_data:
    print(key[0])

# for key in sorted(art_list_data, key=lambda x:x[0]):
#     print (key, art_list_data[key])

# print(sort_list)


# разбираем ответ сервера
# i = json.loads(r.text)
# print(i['birthday'])
# print(i['sortable_name'])