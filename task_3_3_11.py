import sys
import re
pat = r"\b(\w+)\1\b"
stts = list()
for line in sys.stdin:
    line = line.rstrip()
    # process line
    if re.match(pat, line):
        print(line)