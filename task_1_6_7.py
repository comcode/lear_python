def getParents(parent, child):
    if (parent == child) or (parent in myDist[child]):
        return "Yes"
    for i in myDist[child]:
        if parent in myDist[child]:
            return 'Yes'
    for i in myDist[child]:
        if getParents(parent, i)== 'Yes':
            return 'Yes'
    return 'No'

myDist = {}
n = int(input())
k = 0
while k < n:
    put = input().split(' : ')
    if len(put) == 1:
        myDist[put[0]] = []
    else:
        myDist[put[0]] = put[1].split()
    k += 1

n = int(input())
k = 0
while k < n:
    parent, child = input().split()
    print(getParents(parent, child))
    k+=1