# ВЫПОЛНЕНА

from xml.etree import ElementTree
str_xml = input()
root = ElementTree.fromstring(str_xml)
cost = {"red": 0, "green": 0, "blue": 0}

def mytree (root, level=1):
    global cost
    cost[root.attrib['color']] += level
    for i in root:
        mytree(i,level+1)

mytree(root)
print(cost['red'], cost['green'], cost['blue'])