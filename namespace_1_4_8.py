namespaces = {'global': {'parent': None, 'child': [] , 'vars': []}}


def create(ns, par):
    global namespaces
    namespaces.setdefault(ns, {'parent': par, 'child': [], 'vars': []})
    if par in namespaces.keys():
        namespaces[par]['child'].append(ns)


def add(ns, var):
    global namespaces
    namespaces[ns]['vars'].append(var)


def get(ns, var):
    global namespaces
    if ns == None:
        print('None')
        return
    if var in namespaces[ns]['vars']:
        print(ns)
    else:
        get(namespaces[ns]['parent'], var)


counter = int(input())
command_list = []
while counter > 0:
    text = str(input())
    command_list.append(text.split(' '))
    counter -= 1

for i in command_list:
    if i[0] == 'add':
        add(i[1], i[2])
    elif i[0] == 'create':
        create(i[1], i[2])
    elif i[0] == 'get':
        get(i[1], i[2])
    else:
        print('Херовый программис')