n, k = map(int, input().split())
def someC (n, k):
    if 1 <= n <= 100 and 1 <= k <= 100:
        return someC(n-1,k) + someC(n-1,k-1)
    elif k > n:
        return 0
    else:
        return 1
print(someC(n,k))