# pattern = r"<a.*href=\"(.+)\""
#
#  Через findall находишь все ссылки
#
#  По каждой из них делаешь запрос
#
#  Результат этого запроса тоже засовываешь в findall с этим же регекспом.
#
#  Если вторая ссылка (Б) в том, что тебе вернул findall - возвращаешь Yes

import re
import requests
urls = dict(A = input(), B = input())
pater = r'(?:a) (?:.*)(?:href=|href =|href= |href = )(?:\"|\')(http://|https://[a-z_-]+[.\-_\w:/]*)'
#pattern = r"<a.*href=\"(.+)\""
respA = requests.get(urls['A'])
for i in re.findall(pater,respA.text):
    respB = requests.get(i)
    if urls['B'] in re.findall(pater,respB.text):
        print("Yes")
        exit()
print("No")