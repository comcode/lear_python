class Buffer:
    def __init__(self):
        self.summ = 0
        self.roster = []

    def add(self, *a):
        for item in a:
            self.roster.append(item)
            if len(self.roster) % 5 == 0:
                self.summ = 0
                for v in range(5):
                    self.summ += self.roster.pop()
                print(self.summ)

    def get_current_part(self):
        return self.roster